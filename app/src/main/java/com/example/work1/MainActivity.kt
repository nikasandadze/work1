package com.example.work1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.work1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnfragment1.setOnClickListener {

            replaceFragment(Fragment1())


        }

        binding.btnfragment2.setOnClickListener {


            replaceFragment(Fragment2())

        }

        binding.btnfragment3.setOnClickListener {


            replaceFragment(Fragment3())

        }

        binding.btnfragment4.setOnClickListener {



            replaceFragment(Fragment4())
        }




    }

    private fun replaceFragment(fragment : Fragment) {

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer,fragment)
        fragmentTransaction.commit()

    }
}